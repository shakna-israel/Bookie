from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Bookie'
settings.subtitle = 'developed by jm | Design'
settings.author = 'shakna-israel'
settings.author_email = 'bookie@shaknaisrael.com'
settings.keywords = 'book open source development writing write fiction non-fiction'
settings.description = 'Open Source Book Repository'
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '277b9e5d-4b4f-47fc-87bc-8456e661f409'
settings.email_server = 'logging'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
